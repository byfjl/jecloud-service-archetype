# JECloud微服务骨架项目

## 项目简介

JECloud微服务项目骨架是创建JECloud微服务项目的一种快捷方式，它使用Maven项目骨架模板快速生成微服务项目，用户只需少量配置即可快速创建JECloud微服务业务项目。

> <span style="color:red;">*请注意，此项目需要依赖JECloud平台才可使用，请预先安装JECloud低代码平台并确保网络互通。</span>

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 导入项目骨架

用户可以install此项目骨架到本地仓库，同样也可以使用JECloud官方Maven私服导入项目骨架

### 本地构建项目并导入项目骨架

1. git clone 此项目
2. 执行mvn clean install,安装项目骨架至本地仓库。
3. File --> New Project，选择Maven项目。
4. 勾选Create from archetype。
5. 点击Add Archetype
6. 在弹出框输出如下内容：
   1. GroupId: jecloud
   2. ArtifactId: jecloud-service-archetype
   3. Version: 项目骨架版本，初始版本为1.0,具体参照pom版本。
7. 在archetype列表可以找到jecloud，添加成功！

### 使用官方Maven私服导入项目骨架

1. File --> New Project，选择Maven项目。
2. 勾选Create from archetype。
3. 点击Add Archetype
4. 在弹出框输出如下内容：
   1. GroupId: jecloud
   2. ArtifactId: jecloud-service-archetype
   3. Version: 项目骨架版本，初始版本为1.0,具体可以在maven私服中找到。
   4. Repository: http://maven.jepaas.com
5. 在archetype列表可以找到jecloud，添加成功！

## 配置settings.xml

[请参照此配置](./docs/settings.md)

> 请确保maven私服配置正确，否则会导致依赖包下载失败的问题。[点击访问JECloud官方Maven私服](http://maven.jepaas.com)

## 使用Idea快速创建项目

1. File --> New Project，选择Maven项目。
2. 勾选Create from archetype。
3. 选择maven骨架，jecloud:jecloud-service-archetype，点击下一步。
4. 设置项目GroupId，可以任意输入，建议为jecloud，保持与技术平台一致。
5. 设置ArtifactId，保持项目名即可。
6. 完成创建。

## JECloud自定义微服务

[从零创建一个自定义微服务模块](https://doc.jepaas.com/docs/je-doc-jecloud-help/je-doc-jecloud-help-1e3u23imujdiv)

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)