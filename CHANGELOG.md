## [2.2.2](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.2.1...v2.2.2) (2023-12-29)



## [2.2.1](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.2.0...v2.2.1) (2023-12-15)



# [2.2.0](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.1.1...v2.2.0) (2023-11-15)



## [2.1.1](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.1.0...v2.1.1) (2023-10-27)



# [2.1.0](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.9...v2.1.0) (2023-10-20)



## [2.0.9](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.8...v2.0.9) (2023-09-22)



## [2.0.8](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.7...v2.0.8) (2023-09-15)



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.6...v1.4.1) (2023-09-08)



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.5...v1.4.1) (2023-09-01)



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.4...v1.4.1) (2023-08-25)



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/opensource/backend/jecloud-service-archetype/compare/v2.0.3...v1.4.1) (2023-08-18)



